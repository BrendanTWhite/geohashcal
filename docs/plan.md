# Objects



## Calendar

### Notes
- Never saved to database

### Function
- for each day from 14 days in the past to 7 in the future:
- for each of the nine graticules (including surrounding):
- get firstOrCreate Expedition for that date / graticule
- if expeditionPosition() is not empty / null...
- ...and if it's within N kilometers of the user's location...
- ...then add it to the calendar
- return calendar


## Expedition

### Fields
- id
- int gratLat
- int gratLong
- date expeditionDate
- foreign key dowJonesDateId

### Functions
#### expeditionPosition()
- returns lat/long array if possible
- returns null if not
- gets the correct DJ date from field in db
- Calculates exact location from [the algorithm](http://wiki.xkcd.com/geohashing/The_Algorithm)

#### getDowJonesDate()
- calculated in [boot()](https://laravel-news.com/eloquent-tips-tricks)
- Saves Dow Jones date for this expedition to field in db
- considers -30 degrees rule
- considers replacement dates for [DJ holidays](http://wiki.xkcd.com/geohashing/Dow_holiday)
- considers [-0 issue](http://wiki.xkcd.com/geohashing/-0_Issue)


## DowJonesDate

### Fields
- id
- date dowJonesDate
- real openingPrice (nullable)
- datetime dowJonesOpenMoment
- datetime lastCheck (nullable)

### Functions
#### Calculate dowJonesOpenMoment 
- in [boot()](https://laravel-news.com/eloquent-tips-tricks)

#### getOpeningPrice
- If the price is already in the db, just return it...
- ...else if now < dowJonesOpenMoment, return null...
- ...else if now < lastCheck + 5 mins, return null...
- ...else try to get cost from web service.
- If we successfully get the cost...
- ...then save it to the db and return it...
- ...else update lastCheck and return null.




# Pages

## Main Page
### Bootstrap Template
- Grab a simple one - perhaps the [Sticky Footer](https://getbootstrap.com/docs/4.1/examples/sticky-footer/) one from getbootstrap.

### Ask the user for their location
- Use standard [geolocation](https://www.w3schools.com/html/html5_geolocation.asp) API
- Use [about:config](https://security.stackexchange.com/questions/147166/how-can-you-fake-geolocation-in-firefox) to test other locations

### Ask how far they want to go
- perhaps a drop-down with a few options for kilometers (with approx miles in brackets)

### Lastly, present the URL for the iCal calendar
- something like www.whatever.com/calendar/mylatitude/mylongitude/maxdistance


## Calendar

### Get calendars as above

### Build an iCal calendar 
- Use [markuspoerschke/iCal](https://github.com/markuspoerschke/iCal)
