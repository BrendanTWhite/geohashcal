<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDowJonesDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dow_jones_dates', function (Blueprint $table) {
            $table->increments('id');

            $table->date('dow_jones_date')->unique();
            $table->decimal('opening_price', 10, 2)->nullable();
            $table->dateTime('dow_jones_opening_moment');
            $table->dateTime('last_check')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dow_jones_dates');
    }
}
