<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add2022Through2030ToHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('holidays', function (Blueprint $table) {

            DB::table('holidays')->where([
                ['holiday_date', '>=', new Carbon('1 January 2022')],
                ['holiday_date', '<=', new Carbon('31 December 2030')],
            ])->delete();

            DB::table('holidays')->insert([
                [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 17 January 2022'),
                    'substitute_date' => new Carbon('Friday 14 January 2022'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 21 February 2022'),
                    'substitute_date' => new Carbon('Friday 18 February 2022'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 15 April 2022'),
                    'substitute_date' => new Carbon('Thursday 14 April 2022'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 30 May 2022'),
                    'substitute_date' => new Carbon('Friday 27 May 2022'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Monday 4 July 2022'),
                    'substitute_date' => new Carbon('Friday 1 July 2022'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 5 September 2022'),
                    'substitute_date' => new Carbon('Friday 2 September 2022'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 24 November 2022'),
                    'substitute_date' => new Carbon('Wednesday 23 November 2022'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Monday 26 December 2022'),
                    'substitute_date' => new Carbon('Friday 23 December 2022'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Monday 2 January 2023'),
                    'substitute_date' => new Carbon('Friday 30 December 2022'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 16 January 2023'),
                    'substitute_date' => new Carbon('Friday 13 January 2023'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 20 February 2023'),
                    'substitute_date' => new Carbon('Friday 17 February 2023'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 7 April 2023'),
                    'substitute_date' => new Carbon('Thursday 6 April 2023'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 29 May 2023'),
                    'substitute_date' => new Carbon('Friday 26 May 2023'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Tuesday 4 July 2023'),
                    'substitute_date' => new Carbon('Monday 3 July 2023'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 4 September 2023'),
                    'substitute_date' => new Carbon('Friday 1 September 2023'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 23 November 2023'),
                    'substitute_date' => new Carbon('Wednesday 22 November 2023'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Monday 25 December 2023'),
                    'substitute_date' => new Carbon('Friday 22 December 2023'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Monday 1 January 2024'),
                    'substitute_date' => new Carbon('Friday 29 December 2023'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 15 January 2024'),
                    'substitute_date' => new Carbon('Friday 12 January 2024'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 19 February 2024'),
                    'substitute_date' => new Carbon('Friday 16 February 2024'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 29 March 2024'),
                    'substitute_date' => new Carbon('Thursday 28 March 2024'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 27 May 2024'),
                    'substitute_date' => new Carbon('Friday 24 May 2024'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Thursday 4 July 2024'),
                    'substitute_date' => new Carbon('Wednesday 3 July 2024'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 2 September 2024'),
                    'substitute_date' => new Carbon('Friday 30 August 2024'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 28 November 2024'),
                    'substitute_date' => new Carbon('Wednesday 27 November 2024'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Wednesday 25 December 2024'),
                    'substitute_date' => new Carbon('Tuesday 24 December 2024'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Wednesday 1 January 2025'),
                    'substitute_date' => new Carbon('Tuesday 31 December 2024'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 20 January 2025'),
                    'substitute_date' => new Carbon('Friday 17 January 2025'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 17 February 2025'),
                    'substitute_date' => new Carbon('Friday 14 February 2025'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 18 April 2025'),
                    'substitute_date' => new Carbon('Thursday 17 April 2025'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 26 May 2025'),
                    'substitute_date' => new Carbon('Friday 23 May 2025'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Friday 4 July 2025'),
                    'substitute_date' => new Carbon('Thursday 3 July 2025'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 1 September 2025'),
                    'substitute_date' => new Carbon('Friday 29 August 2025'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 27 November 2025'),
                    'substitute_date' => new Carbon('Wednesday 26 November 2025'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Thursday 25 December 2025'),
                    'substitute_date' => new Carbon('Wednesday 24 December 2025'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Thursday 1 January 2026'),
                    'substitute_date' => new Carbon('Wednesday 31 December 2025'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 19 January 2026'),
                    'substitute_date' => new Carbon('Friday 16 January 2026'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 16 February 2026'),
                    'substitute_date' => new Carbon('Friday 13 February 2026'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 3 April 2026'),
                    'substitute_date' => new Carbon('Thursday 2 April 2026'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 25 May 2026'),
                    'substitute_date' => new Carbon('Friday 22 May 2026'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Friday 3 July 2026'),
                    'substitute_date' => new Carbon('Thursday 2 July 2026'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 7 September 2026'),
                    'substitute_date' => new Carbon('Friday 4 September 2026'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 26 November 2026'),
                    'substitute_date' => new Carbon('Wednesday 25 November 2026'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Friday 25 December 2026'),
                    'substitute_date' => new Carbon('Thursday 24 December 2026'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Friday 1 January 2027'),
                    'substitute_date' => new Carbon('Thursday 31 December 2026'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 18 January 2027'),
                    'substitute_date' => new Carbon('Friday 15 January 2027'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 15 February 2027'),
                    'substitute_date' => new Carbon('Friday 12 February 2027'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 26 March 2027'),
                    'substitute_date' => new Carbon('Thursday 25 March 2027'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 31 May 2027'),
                    'substitute_date' => new Carbon('Friday 28 May 2027'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Monday 5 July 2027'),
                    'substitute_date' => new Carbon('Friday 2 July 2027'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 6 September 2027'),
                    'substitute_date' => new Carbon('Friday 3 September 2027'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 25 November 2027'),
                    'substitute_date' => new Carbon('Wednesday 24 November 2027'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Friday 24 December 2027'),
                    'substitute_date' => new Carbon('Thursday 23 December 2027'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 17 January 2028'),
                    'substitute_date' => new Carbon('Friday 14 January 2028'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 21 February 2028'),
                    'substitute_date' => new Carbon('Friday 18 February 2028'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 14 April 2028'),
                    'substitute_date' => new Carbon('Thursday 13 April 2028'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 29 May 2028'),
                    'substitute_date' => new Carbon('Friday 26 May 2028'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Tuesday 4 July 2028'),
                    'substitute_date' => new Carbon('Monday 3 July 2028'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 4 September 2028'),
                    'substitute_date' => new Carbon('Friday 1 September 2028'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 23 November 2028'),
                    'substitute_date' => new Carbon('Wednesday 22 November 2028'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Monday 25 December 2028'),
                    'substitute_date' => new Carbon('Friday 22 December 2028'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Monday 1 January 2029'),
                    'substitute_date' => new Carbon('Friday 29 December 2028'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 15 January 2029'),
                    'substitute_date' => new Carbon('Friday 12 January 2029'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 19 February 2029'),
                    'substitute_date' => new Carbon('Friday 16 February 2029'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 30 March 2029'),
                    'substitute_date' => new Carbon('Thursday 29 March 2029'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 28 May 2029'),
                    'substitute_date' => new Carbon('Friday 25 May 2029'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Wednesday 4 July 2029'),
                    'substitute_date' => new Carbon('Tuesday 3 July 2029'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 3 September 2029'),
                    'substitute_date' => new Carbon('Friday 31 August 2029'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 22 November 2029'),
                    'substitute_date' => new Carbon('Wednesday 21 November 2029'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Tuesday 25 December 2029'),
                    'substitute_date' => new Carbon('Monday 24 December 2029'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Tuesday 1 January 2030'),
                    'substitute_date' => new Carbon('Monday 31 December 2029'),
                ], [
                    'description'     => 'Martin Luther King, Jr. Day',
                    'holiday_date'    => new Carbon('Monday 21 January 2030'),
                    'substitute_date' => new Carbon('Friday 18 January 2030'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 18 February 2030'),
                    'substitute_date' => new Carbon('Friday 15 February 2030'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 19 April 2030'),
                    'substitute_date' => new Carbon('Thursday 18 April 2030'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 27 May 2030'),
                    'substitute_date' => new Carbon('Friday 24 May 2030'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Thursday 4 July 2030'),
                    'substitute_date' => new Carbon('Wednesday 3 July 2030'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 2 September 2030'),
                    'substitute_date' => new Carbon('Friday 30 August 2030'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 28 November 2030'),
                    'substitute_date' => new Carbon('Wednesday 27 November 2030'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Wednesday 25 December 2030'),
                    'substitute_date' => new Carbon('Tuesday 24 December 2030'),
                ]
     
            ]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holidays', function (Blueprint $table) {

            DB::table('holidays')->where([
                ['holiday_date', '>=', new Carbon('1 January 2022')],
                ['holiday_date', '<=', new Carbon('31 December 2030')],
            ])->delete();

        });
    }
}
