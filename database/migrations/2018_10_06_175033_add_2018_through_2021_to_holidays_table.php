<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Add2018Through2021ToHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('holidays', function (Blueprint $table) {

            DB::table('holidays')->where([
                ['holiday_date', '>=', new Carbon('1 January 2018')],
                ['holiday_date', '<=', new Carbon('31 December 2021')],
            ])->delete();

            DB::table('holidays')->insert([
                [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Monday 1 January 2018'),
                    'substitute_date' => new Carbon('Friday 29 December 2017'),
                ], [
                    'description'     => 'Martin Luther King,Jr. Day',
                    'holiday_date'    => new Carbon('Monday 15 January 2018'),
                    'substitute_date' => new Carbon('Friday 12 January 2018'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 19 February 2018'),
                    'substitute_date' => new Carbon('Friday 16 February 2018'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 30 March 2018'),
                    'substitute_date' => new Carbon('Thursday 29 March 2018'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 28 May 2018'),
                    'substitute_date' => new Carbon('Friday 25 May 2018'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Wednesday 4 July 2018'),
                    'substitute_date' => new Carbon('Tuesday 3 July 2018'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 3 September 2018'),
                    'substitute_date' => new Carbon('Friday 31 August 2018'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 22 November 2018'),
                    'substitute_date' => new Carbon('Wednesday 21 November 2018'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Tuesday 25 December 2018'),
                    'substitute_date' => new Carbon('Monday 24 December 2018'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Tuesday 1 January 2019'),
                    'substitute_date' => new Carbon('Monday 31 December 2018'),
                ], [
                    'description'     => 'Martin Luther King,Jr. Day',
                    'holiday_date'    => new Carbon('Monday 21 January 2019'),
                    'substitute_date' => new Carbon('Friday 18 January 2019'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 18 February 2019'),
                    'substitute_date' => new Carbon('Friday 15 February 2019'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 19 April 2019'),
                    'substitute_date' => new Carbon('Thursday 18 April 2019'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 27 May 2019'),
                    'substitute_date' => new Carbon('Friday 24 May 2019'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Thursday 4 July 2019'),
                    'substitute_date' => new Carbon('Wednesday 3 July 2019'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 2 September 2019'),
                    'substitute_date' => new Carbon('Friday 30 August 2019'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 28 November 2019'),
                    'substitute_date' => new Carbon('Wednesday 27 November 2019'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Wednesday 25 December 2019'),
                    'substitute_date' => new Carbon('Tuesday 24 December 2019'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Wednesday 1 January 2020'),
                    'substitute_date' => new Carbon('Tuesday 31 December 2019'),
                ], [
                    'description'     => 'Martin Luther King,Jr. Day',
                    'holiday_date'    => new Carbon('Monday 20 January 2020'),
                    'substitute_date' => new Carbon('Friday 17 January 2020'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 17 February 2020'),
                    'substitute_date' => new Carbon('Friday 14 February 2020'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 10 April 2020'),
                    'substitute_date' => new Carbon('Thursday 9 April 2020'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 25 May 2020'),
                    'substitute_date' => new Carbon('Friday 22 May 2020'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Friday 3 July 2020'),
                    'substitute_date' => new Carbon('Thursday 2 July 2020'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 7 September 2020'),
                    'substitute_date' => new Carbon('Friday 4 September 2020'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 26 November 2020'),
                    'substitute_date' => new Carbon('Wednesday 25 November 2020'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Friday 25 December 2020'),
                    'substitute_date' => new Carbon('Thursday 24 December 2020'),
                ], [
                    'description'     => 'New Year\'s Day',
                    'holiday_date'    => new Carbon('Friday 1 January 2021'),
                    'substitute_date' => new Carbon('Thursday 31 December 2020'),
                ], [
                    'description'     => 'Martin Luther King,Jr. Day',
                    'holiday_date'    => new Carbon('Monday 18 January 2021'),
                    'substitute_date' => new Carbon('Friday 15 January 2021'),
                ], [
                    'description'     => 'Washington\'s Birthday',
                    'holiday_date'    => new Carbon('Monday 15 February 2021'),
                    'substitute_date' => new Carbon('Friday 12 February 2021'),
                ], [
                    'description'     => 'Good Friday',
                    'holiday_date'    => new Carbon('Friday 2 April 2021'),
                    'substitute_date' => new Carbon('Thursday 1 April 2021'),
                ], [
                    'description'     => 'Memorial Day',
                    'holiday_date'    => new Carbon('Monday 31 May 2021'),
                    'substitute_date' => new Carbon('Friday 28 May 2021'),
                ], [
                    'description'     => 'Independence Day',
                    'holiday_date'    => new Carbon('Monday 5 July 2021'),
                    'substitute_date' => new Carbon('Friday 2 July 2021'),
                ], [
                    'description'     => 'Labor Day',
                    'holiday_date'    => new Carbon('Monday 6 September 2021'),
                    'substitute_date' => new Carbon('Friday 3 September 2021'),
                ], [
                    'description'     => 'Thanksgiving Day',
                    'holiday_date'    => new Carbon('Thursday 25 November 2021'),
                    'substitute_date' => new Carbon('Wednesday 24 November 2021'),
                ], [
                    'description'     => 'Christmas',
                    'holiday_date'    => new Carbon('Friday 24 December 2021'),
                    'substitute_date' => new Carbon('Thursday 23 December 2021'),
                ]
            ]);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holidays', function (Blueprint $table) {

            DB::table('holidays')->where([
                ['holiday_date', '>=', new Carbon('1 January 2018')],
                ['holiday_date', '<=', new Carbon('31 December 2021')],
            ])->delete();

        });
    }
}
