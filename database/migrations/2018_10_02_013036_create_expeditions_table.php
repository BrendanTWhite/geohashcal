<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpeditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expeditions', function (Blueprint $table) {
            $table->increments('id');

            $table->char('grat_lat', 4);
            $table->char('grat_long', 4);
            $table->date('expedition_date');

            $table->unsignedInteger('dow_jones_date_id');
            $table->foreign('dow_jones_date_id')
                ->references('id')->on('dow_jones_dates');

            $table->decimal('exp_lat', 10, 6)->nullable();
            $table->decimal('exp_long', 10, 6)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expeditions');
    }
}
