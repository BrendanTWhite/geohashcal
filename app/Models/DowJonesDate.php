<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class DowJonesDate extends Model
{

	public static function boot()
	{
		parent::boot();
		self::creating(function ($model) {
			$model->dow_jones_opening_moment =

			// The moment the DJ opens on a given date
			// is 9.30am New York time, according to
			// http://wiki.xkcd.com/geohashing/Eastern_Time

			Carbon::create(
				$model->dow_jones_date->year,
				$model->dow_jones_date->month,
				$model->dow_jones_date->day,
				9, 30, 0,   // hour, minute, second
				'America/New_York'
			)->setTimezone('UTC');

		});
	} // end function boot()


	public function getOpeningPrice(): ?float {
		Log::debug('-- getOpeningPrice ');
		Log::debug('existing dj date '.$this->dow_jones_date->toCookieString());
		Log::debug('existing price '.$this->opening_price);
		Log::debug('existing opening moment '.$this->dow_jones_opening_moment->toCookieString());
		if ($this->last_check) {
			Log::debug('existing last check '.$this->last_check->toCookieString());
		} else {
			Log::debug('existing last check not available');
		}
		Log::debug('Now '.Carbon::now()->toCookieString());

		// If we already have the price, just return it
		if ($this->opening_price) {
			Log::debug('already got it, just return it');
			return $this->opening_price;

		}

		// If it's not yet opening time, return null
		else if (Carbon::now() < $this->dow_jones_opening_moment) {
			Log::debug('not yet at opening moment');
			return null;
		}

		// If we checked the web service very recently, don't check again
		else if (($this->last_check) // ie we have already checked at least once so have a last-check date
				and (Carbon::now() < $this->last_check->addMinutes(2))) {
			Log::debug('checked a short while ago, won\'t check again just yet');
			return null;
		}

		// If we get this far, attempt to get the DJ price
		else {
			$this->opening_price =
				$this->obtainPriceFromWebServiceFor($this->dow_jones_date);
			$this->last_check = Carbon::now();
			$this->save();
			Log::debug('new price '.$this->opening_price);
			Log::debug('new last check '.$this->last_check->toCookieString());
		}

		// Finally, return the opening price (such as we have)
		return $this->opening_price;

	} // end getOpeningPrice


    private static function obtainPriceFromWebServiceFor(Carbon $theDate): ?float {
    	Log::debug('-- obtainPriceFromWebServiceFor '.$theDate->toCookieString());

		// The URL of the web service is of the form
		// http://geo.crox.net/djia/%Y/%m/%d
		// according to the doco at
		// http://wiki.xkcd.com/geohashing/Dow_Jones_Industrial_Average
		$theURL = 'http://geo.crox.net/djia/'
		.$theDate->year.'/'
		.$theDate->month.'/'
		.$theDate->day;
		Log::debug('web service URL: '.$theURL);

		try {
			$theResults = @file_get_contents($theURL);
		}
		catch (Exception $e) {
		    $theResults = null;
		}
		Log::debug('results: '.$theResults);

		// It should be either a dollar amount (with cents)
		// or text, eg "data not available yet".

		// So if it's numeric, it's almost certainly
		// the correct opening price.
		if (is_numeric($theResults)) {
			return (float)$theResults;

		// If not, we don't have the right price yet
		// so just return null for now
		} else {

			return null;
		}

	} // end obtainPriceFromWebServiceFor


	protected $hidden = ['opening_price'];
	protected $fillable = ['dow_jones_date'];
	protected $dates = ['dow_jones_date','dow_jones_opening_moment','last_check'];
} // end class DowJonesDate
