<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class Expedition extends Model
{

	public static function boot()
	{
		parent::boot();
		self::creating(function ($model) {

			$dateToUse = self::determineWhichDJDateToUseFor(
					$model->grat_lat,
					$model->grat_long,
					$model->expedition_date
				);

			$model->dow_jones_date_id =	DowJonesDate::firstOrCreate(['dow_jones_date' => $dateToUse])->id;

		});
	} // end function boot()


	public function getExpLatitude() {
		Log::debug('-- Attempting to return Exp Lat for '.$this->expedition_date->toDateString()
			. ', ' . $this->grat_lat . ', ' . $this->grat_long );

		// If we already have it calculated, just return it
		if ($this->exp_lat) {
			Log::debug('Already got '.$this->exp_lat.' so just use that.');
			return $this->exp_lat;
		}

		// If not, attempt to calculate it now, and then return whatever we've got
		else {
			Log::debug('Don\'t have one yet, so we\'ll attempt to calculate it.');
			$this->calcCoordinates();
			return $this->exp_lat;
		}

	} // end getExpLatitude()


	public function getExpLongitude() {
		Log::debug('-- Attempting to return Exp Long for '.$this->expedition_date->toDateString()
			. ', ' . $this->grat_lat . ', ' . $this->grat_long );

		// If we already have it calculated, just return it
		if ($this->exp_long) {
			Log::debug('Already got '.$this->exp_long.' so just use that.');
			return $this->exp_long;
		}

		// If not, attempt to calculate it now, and then return whatever we've got
		else {
			Log::debug('Don\'t have one yet, so we\'ll attempt to calculate it.');
			$this->calcCoordinates();
			return $this->exp_long;
		}

	} // end getExpLatitude()


	private function calcCoordinates(){
		// See algorithm at http://wiki.xkcd.com/geohashing/The_Algorithm
		Log::debug('-- Attempting to calculate Exp Lat for '.$this->expedition_date->toDateString()
			. ', ' . $this->grat_lat . ', ' . $this->grat_long );

		// First, check if the DJ opening price is available
		// If not, there's nothing useful we can do, so just quit
		$theDJprice = $this->dow_jones_date->getOpeningPrice();
		if (!$theDJprice) {
			Log::debug('No DJ price for that date yet, so can\'t calcuate coordinates.');
			return;
		}

		// If we have a DJ opening price, calculate the key
		$key =
			$this->expedition_date->toDateString()
			. '-' . $theDJprice;
		Log::debug('Key is '.$key);

		// Then, calculat the hash of the key
		$hash = md5($key);
		Log::debug('Hash is '.$hash);
		$hexLatHash = substr($hash, 0, 16);
		$hexLongHash = substr($hash, 16, 16);
		Log::debug('hexLatHash is '.$hexLatHash);
		Log::debug('hexLongHash is '.$hexLongHash);

		// Then, calculate the decimal fractions
		// To change four-digit base-10 number 1234 to 0.1234, divide by 10000 (1 with four 0s)
		// To change four-digit hex ABCD to 0.ABCD, divide by hex 10000 (1 with four 0s)
		// To change a sixteen digit hex number 123456789ABCDEF0 to 0.123456789ABCDEF0 ...
		// ... divide by hex number 1 with 16 zeros
		$latFraction  = hexdec($hexLatHash) / hexdec('1' . str_repeat ('0', 16));
		$longFraction = hexdec($hexLongHash) / hexdec('1' . str_repeat ('0', 16));
		Log::debug('Lat  Fraction is '.$latFraction);
		Log::debug('Long Fraction is '.$longFraction);

		// Then assemble the full coordinates
		// $this->grat_lat is text eg '-0'
		// and $latFraction is a real eg 0.62477195620672
		$this->exp_lat = str_replace('0.', $this->grat_lat.'.', $latFraction);
		$this->exp_long = str_replace('0.', $this->grat_long.'.', $longFraction);
		$this->save();
		Log::debug('Expedition Latitude is '.$this->exp_lat);
		Log::debug('Expedition Longitude is '.$this->exp_long);

	} // end getExpLatitude


	private static function determineWhichDJDateToUseFor(
		string $grat_lat,
		string $grat_long,
		Carbon $expedition_date
		): Carbon {

		Log::debug('determining which DJ date to use for '
			.$expedition_date->toCookieString() . ', '
			.$grat_lat .', ' .$grat_long);

		// Start with the assumption that we'll just use
		// the expedition date
		$djDate = $expedition_date;

		// If the 30W rule is in effect, use the DJ from the previous day
		// http://wiki.xkcd.com/geohashing/30W_Time_Zone_Rule
		if ((int)$grat_long >= -30) {
			$djDate = $djDate->subDay();
			Log::debug($grat_long.' is east of -30, so using '.$djDate->toCookieString());
		}

		// If it's a weekend, use the previous Friday
		if ($djDate->isSunday()) {
			$djDate = $djDate->subDays(2);
			Log::debug('That was Sunday, so using '.$djDate->toCookieString());
		} else if ($djDate->isSaturday()) {
			$djDate = $djDate->subDay();
			Log::debug('That was Saturday, so using '.$djDate->toCookieString());
		}

		// If it's a known DJ holiday, use the substitution date
		// http://wiki.xkcd.com/geohashing/Dow_holiday
		$theHol = Holiday::where('holiday_date', $djDate)->first();
		if ($theHol){
			$djDate = $theHol->substitute_date;
			Log::debug('That was a DJ Holiday, so using '.$djDate->toCookieString());
		}

		return $djDate;

	} // end determineWhichDJDateToUseFor



    /**
     * Get the DJ Date for this Expedition.
     */
    public function dow_jones_date()
    {
        return $this->belongsTo('App\Models\DowJonesDate');
    }

	protected $fillable = ['expedition_date','grat_lat','grat_long'];
	protected $dates = ['expedition_date'];
}
