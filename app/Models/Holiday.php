<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
	protected $dates = ['holiday_date', 'substitute_date'];
}
